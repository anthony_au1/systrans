package global.cube;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;

public class App {
    private final static String SYSTRANS_SERVER = "https://cube.mysystran.com:8904";
    private final static String SYSTRANS_KEY = "35bb9fa5-af2b-4f9f-9c0c-4ba90693cda8";

    private static class SupportedLanguage {
        public String source;
        public String target;
        public String languagePair;
        public String translationResource;

        @Override
        public int hashCode() {
            return 31 * source.hashCode() + target.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof SupportedLanguage))
                return false;
            var supportedLanguage = (SupportedLanguage) obj;
            return supportedLanguage.source.equals(source) && supportedLanguage.target.equals(target);
        }
    }

    private Set<SupportedLanguage> getSupportedLanguages(HttpClient httpClient)
            throws URISyntaxException, IOException, InterruptedException {
        Set<SupportedLanguage> result = new HashSet<>();

        var request = HttpRequest.newBuilder().uri(new URI(
                String.format("%s/translation/supportedLanguages?key=%s", App.SYSTRANS_SERVER, App.SYSTRANS_KEY))).GET()
                .build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        var root = new JSONObject(response.body());
        var languagePairs = new JSONArray(root.getJSONArray("languagePairs"));
        for (int i = 0; i < languagePairs.length(); i++) {
            var languagePair = languagePairs.getJSONObject(i);
            var source = languagePair.getString("source");
            var target = languagePair.getString("target");

            var supportedLanguage = new SupportedLanguage();
            supportedLanguage.source = source;
            supportedLanguage.target = target;
            supportedLanguage.languagePair = String.format("RM/%s-%s", source, target);

            result.add(supportedLanguage);
        }

        return result;
    }

    private Set<String> getExistingProfiles(HttpClient httpClient)
            throws URISyntaxException, IOException, InterruptedException {
        Set<String> result = new HashSet<>();

        var request = HttpRequest.newBuilder()
                .uri(new URI(String.format("%s/profiles?key=%s", App.SYSTRANS_SERVER, App.SYSTRANS_KEY))).GET().build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        var root = new JSONObject(response.body());
        var profiles = new JSONArray(root.getJSONArray("profiles"));
        for (int i = 0; i < profiles.length(); i++) {
            var profile = profiles.getJSONObject(i);
            var name = profile.getString("name");

            if (name.startsWith("RM/"))
                result.add(name);
        }

        return result;
    }

    private Map<String, SupportedLanguage> getTranslationResources(HttpClient httpClient)
            throws URISyntaxException, IOException, InterruptedException {
        Map<String, SupportedLanguage> result = new HashMap<>();

        var request = HttpRequest.newBuilder()
                .uri(new URI(String.format("%s/translationResources?key=%s", App.SYSTRANS_SERVER, App.SYSTRANS_KEY)))
                .GET().build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        var root = new JSONObject(response.body());
        var translationResources = new JSONArray(root.getJSONArray("translationResources"));
        for (int i = 0; i < translationResources.length(); i++) {
            var translationResource = translationResources.getJSONObject(i);
            var source = translationResource.getString("source");
            var target = translationResource.getString("target");
            var id = translationResource.getString("id");

            var supportedLanguage = new SupportedLanguage();
            supportedLanguage.source = source;
            supportedLanguage.target = target;
            supportedLanguage.translationResource = id;
            supportedLanguage.languagePair = String.format("RM/%s-%s", source, target);

            result.put(supportedLanguage.languagePair, supportedLanguage);
        }

        return result;
    }

    private String createCorpus(SupportedLanguage language, HttpClient httpClient)
            throws URISyntaxException, IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
                .uri(new URI(String.format("%s/resources/corpus/add?key=%s&name=%s&sourceLang=%s&targetLang=%s",
                        App.SYSTRANS_SERVER, App.SYSTRANS_KEY, language.languagePair, language.source,
                        language.target)))
                .GET().build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        var root = new JSONObject(response.body());
        var corpus = root.getJSONObject("corpus");
        var id = corpus.getString("id");

        return id;
    }

    private void deleteCorpus(String corpusId, HttpClient httpClient)
            throws URISyntaxException, IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
                .uri(new URI(String.format("%s/resources/corpus/delete?key=%s&corpusId=%s", App.SYSTRANS_SERVER,
                        App.SYSTRANS_KEY, corpusId)))
                .GET().build();
        httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }

    private String createProfile(SupportedLanguage language, String corpusId, HttpClient httpClient)
            throws URISyntaxException, IOException, InterruptedException {
        var request = HttpRequest.newBuilder().uri(new URI(String.format(
                "%s/profiles/create?key=%s&name=%s&source=%s&target=%s&domain=%s&translationMemories=%s&translationResourceId=%s",
                App.SYSTRANS_SERVER, App.SYSTRANS_KEY, language.languagePair, language.source, language.target, "RM",
                corpusId, language.translationResource))).GET().build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        var root = new JSONObject(response.body());
        var id = root.getString("id");

        return id;
    }

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        var app = new App();
        var httpClient = HttpClient.newHttpClient();

        // get all systrans supported languages
        var supportedLanguages = app.getSupportedLanguages(httpClient);

        // get all existing profiles
        var existingProfiles = app.getExistingProfiles(httpClient);

        if (supportedLanguages.size() == 0) {
            System.out.println("no supported languages");
            System.exit(0);
        }

        // remove already existing languages
        List<SupportedLanguage> notExistingLanguages = supportedLanguages.stream()
                .filter(sl -> !existingProfiles.contains(sl.languagePair)).collect(Collectors.toList());

        if (notExistingLanguages.size() == 0) {
            System.out.println("no new languages");
            System.exit(0);
        }

        // find all translation resources
        var translationResources = app.getTranslationResources(httpClient);

        if (translationResources.size() == 0) {
            System.out.println("no supported translation resources");
            System.exit(0);
        }

        List<SupportedLanguage> languagesToCreate = new ArrayList<>();
        for (SupportedLanguage language : notExistingLanguages) {
            var translationResource = translationResources.get(language.languagePair);
            if (translationResource != null) {
                language.translationResource = translationResource.translationResource;
                languagesToCreate.add(language);
            } else {
                // here we might have a case of dialect, so if we dont' support it then 
                // fallback is to use a parent language
                var source = language.source.split("-")[0];
                var target = language.target.split("-")[0];
                var parentLang = String.format("RM/%s-%s", source, target);
             
                var translationResource2 = translationResources.get(parentLang);
                if (translationResource2 != null) {
                    language.source = source;
                    language.target = target;
                    language.translationResource = translationResource2.translationResource;
                    languagesToCreate.add(language);
                } else {
                    System.out.printf("Translation resource for language pair = %s not found, we skipped this pair%n",
                            language.languagePair);
                }
            }
        }

        if (languagesToCreate.size() == 0) {
            System.out.println("no languages with translation resources");
            System.exit(0);
        }

        for (SupportedLanguage language : languagesToCreate) {
            var corpusId = "";
            try {
                // create a new corpus for a not existing language
                corpusId = app.createCorpus(language, httpClient);

                System.out.println(String.format("New corpus with id = %s, name = %s, source = %s, target = %s",
                        corpusId, language.languagePair, language.source, language.target));

                // create profile associated with a new corpus
                var profileId = app.createProfile(language, corpusId, httpClient);

                System.out.println(String.format(
                        "New profile with id = %s, name = %s, source = %s, target = %s, translationResourceId = %s",
                        profileId, language.languagePair, language.source, language.target,
                        language.translationResource));

            } catch (Exception e) {
                app.deleteCorpus(corpusId, httpClient);
                throw e;
            }
        }
    }
}
